export const constants = {
  FETCH_PROFILE: 'FETCH_PROFILE',
  FETCH_PROFILE_DONE: 'FETCH_PROFILE_DONE',

  FETCH_WALLETS: 'FETCH_WALLETS',
  FETCH_WALLETS_DONE: 'FETCH_WALLETS_DONE',

  FETCH_RATES: 'FETCH_RATES',
  FETCH_RATES_DONE: 'FETCH_RATES_DONE',

  SET_EXCHANGE_FROM_CURRENCY: 'EXCHANGE_FROM_CURRENCY',

  EXCHANGE_MONEY: 'EXCHANGE_MONEY',
};

export const fetchProfile = (data) => ({
  type: constants.FETCH_PROFILE,
  data
});

export const fetchWallets = (data) => ({
  type: constants.FETCH_WALLETS,
  data
});

export const fetchCurrencies = () => ({
  type: constants.FETCH_RATES
});

export const setCurrencyFrom = (data) => ({
  type: constants.SET_EXCHANGE_FROM_CURRENCY,
  data
});

export const exchange = (data) => ({
  type: constants.EXCHANGE_MONEY,
  data
});
