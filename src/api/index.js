const endpoints = {
  FETCH_PROFILE: ['get', '/mocks/profile.json'],
  FETCH_WALLETS: ['get', '/mocks/wallets.json'],
  FETCH_RATES: ['get', 'http://api.fixer.io/latest']
};

const errorResponceTexts = {
  FETCH_RATES_ERROR: 'api.fixer is unavailable'
};

function checkFetchStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    const error = new Error(response.statusText);
    error.response = response;

    throw error;
  }
}

export const api = store => next => action => {
  if (endpoints[action.type]) {
    const url = action.data ? `${endpoints[action.type][1]}?${action.data}` : endpoints[action.type][1];

    fetch(url)
      .then(checkFetchStatus)
      .then((response) => response.json())
      .then((json) => {
        return store.dispatch({
          type: `${action.type}_DONE`,
          data: json,
          status: 'success'
        });
      }).catch(function (e) {
        return store.dispatch({
          type: `${action.type}_DONE`,
          status: 'error',
          error: errorResponceTexts[`${action.type}_ERROR`],
          data: null
        });
    })
  } else {
    return next(action)
  }
};
