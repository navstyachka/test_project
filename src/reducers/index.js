import { constants } from '../actions'
import { setWallets } from '../utilities'
import * as Decimal from 'decimal.js/decimal'

const initialState = {
  profile: null,
  wallets: {},
  rates: {},
  default_currency: '',
  exchange_from: ''
};

// Imitation of server response with new values with the help of decimal.js
function updateWallets(wallets, data) {
  const valueFrom = new Decimal(wallets[data.exchangeFrom.currency]).minus(data.exchangeFrom.amount).toNumber();
  const valueTo = new Decimal(wallets[data.exchangeTo.currency]).plus(data.exchangeTo.amount).toNumber();

  wallets[data.exchangeFrom.currency] = valueFrom;
  wallets[data.exchangeTo.currency] = valueTo;

  return wallets;
}

export default function app(state = initialState, action) {
  const {data} = action;
  switch (action.type) {
    case constants.FETCH_PROFILE_DONE:
      return {
        ...state,
        profile: data,
        default_currency: data.default_currency,
        exchange_from: data.default_currency
      };

    case constants.FETCH_WALLETS_DONE:
      return {
        ...state,
        wallets: setWallets(data.wallets)
      };

    case constants.FETCH_RATES_DONE:
      if (data) {
        const rates = data.rates;

        rates[state.default_currency] = 1;
        action.rates = rates;
      }

      return {
        ...state,
        rates: action
      };

    case constants.SET_EXCHANGE_FROM_CURRENCY:
      return {
        ...state,
        exchange_from: data
      };

    case constants.EXCHANGE_MONEY:
      return {
        ...state,
        wallets: updateWallets(state.wallets, data)
      };

    default:
      return state
  }
}
