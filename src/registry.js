export default {
  index: '/',
  exchange: '/exchange',
  rates: '/rates'
}
