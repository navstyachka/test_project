export function setWallets(wallets) {
  let walletsMap = {};

  wallets.forEach(wallet => walletsMap[wallet.currency] = wallet.amount);

  return walletsMap;
}

export function setFetchCurrenciesParameters(currency) {
  return `base=${currency}`;
}

export function humanizeCurrency(currency) {
  return {
    'USD': '$',
    'EUR': '€',
    'GBP': '£'
  }[currency] || currency;
}
