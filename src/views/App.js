import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { fetchProfile, fetchWallets, fetchCurrencies } from '../actions/index';
import { setFetchCurrenciesParameters } from '../utilities';
import Header from './Header';
import Content from './Content';

class App extends Component {
  fetchCurrenciesInterval = null;

  componentWillMount() {
    this.initializeApp();
  }

  initializeApp() {
    const { dispatch } = this.props;

    dispatch(fetchProfile());
    dispatch(fetchWallets());

    const parameters = setFetchCurrenciesParameters(this.props.default_currency);
    dispatch(fetchCurrencies(parameters));

    this.fetchCurrenciesInterval = setInterval(() => {
      dispatch(fetchCurrencies(parameters));
    }, 10000);
  }

  renderContent() {
    const { rates } = this.props;

    if (rates.status === 'error') {
      return (
        <div className="page__i error">
          {rates.error}
        </div>
      );

    } else {
      return <Content/>;
    }
  }

  render() {
    const { exchange_from, wallets, rates } = this.props;

    if (!exchange_from || !Object.keys(wallets).length || !Object.keys(rates).length) {
      return (
        <div className="loading">Loading...</div>
      );
    }

    return (
      <div className="page">
        <Header/>
        {this.renderContent()}
      </div>
    );
  }
}

App.propTypes = {
  wallets: PropTypes.object,
  exchange_from: PropTypes.string,
  rates: PropTypes.object
};

export default withRouter(connect(store => ({
  exchange_from: store.exchange_from,
  wallets: store.wallets,
  rates: store.rates
}))(App))
