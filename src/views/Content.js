import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import registry from '../registry';
import Exchange from './pages/Exchange';
import Wallet from './pages/Wallet';
import Rates from './pages/Rates';

class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route exact path={registry.index} component={Wallet}/>
        <Route path={registry.exchange} component={Exchange}/>
        <Route path={registry.rates} component={Rates}/>
      </Switch>
    );
  }
}

export default Routes;
