import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom'

import registry from '../registry'

class Header extends Component {
  render() {
    const { profile } = this.props;

    return (
      <header className="header">
        <nav className="nav">
          <NavLink exact className="nav__item" to={registry.index}>Wallet</NavLink>
          <NavLink className="nav__item" to={registry.exchange}>Exchange</NavLink>
        </nav>
        <span className="header__name">Hi, {profile && profile.first_name}!</span>
      </header>
    );
  }
}

Header.propTypes = {
  profile: PropTypes.object
};

export default withRouter(connect(store => ({
  profile: store.profile
}))(Header));