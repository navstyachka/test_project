import React from 'react';

export default function CurrenciesList({ wallets, activeCurrency, setActive }) {
  return (
    <div className="currencies-list">

      {Object.keys(wallets).map(c => (
        activeCurrency !== c && (
          <span key={c} onClick={() => setActive(c)} className="currencies-list__item">
            {c}
          </span>
        ))
      )}

    </div>
  );
}
