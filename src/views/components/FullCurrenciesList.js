import React from 'react';

export default function CurrenciesList({ rates, addCurrency, closePopup }) {
  return (
    <div className="popup">
      <div className="popup__close" onClick={() => closePopup()}/>
      <div className="popup__i">

        {Object.keys(rates).map((currency) =>
          <div key={currency} className="currency" onClick={() => addCurrency(currency)}>
            {currency}
          </div>
        )}

      </div>
    </div>
  );
}
