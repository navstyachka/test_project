import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import fx from 'money'

import registry from '../../registry'
import { humanizeCurrency } from '../../utilities'
import { exchange, setCurrencyFrom } from '../../actions'
import CurrenciesList from '../components/CurrenciesList'

class Exchange extends Component {
  state = {
    exchangeToCurrency: this.getExchangeToCurrency(this.props.exchange_from)[0],
    exchangeFromCurrency: this.props.exchange_from,
    amountFrom: 0,
    amountTo: 0,
    exchangeDirectionIsInverted: false
  };

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps.rates) !== JSON.stringify(this.props.rates)) {
      this.updateExchangeResults();
    }
  }

  getExchangeToCurrency(currencyExchangeFrom) {
    return Object.keys(this.props.wallets).filter(w => w !== currencyExchangeFrom);
  }

  convert(amount = 0, fromCurrency, toCurrency) {
    fx.rates = this.props.rates;
    fx.base = this.props.default_currency;

    return parseFloat(fx.convert(amount, {from: fromCurrency, to: toCurrency}).toFixed(2));
  }

  setExchange(amount, isInverted) {
    const { wallets } = this.props;
    const { exchangeFromCurrency, exchangeToCurrency } = this.state;
    let result;

    amount = this.formatAmount(amount);
    const amountInt = parseFloat(amount);

    if (isInverted) {
      result = this.convert(amount, exchangeToCurrency, exchangeFromCurrency);
      this.setConvertedResults(result, amount, true, result < wallets[exchangeFromCurrency]);

    } else {
      result = this.convert(amount, exchangeFromCurrency, exchangeToCurrency);
      this.setConvertedResults(amount, result, false, amountInt < wallets[exchangeFromCurrency]);

    }
  }

  formatAmount(amount) {
    amount = (amount + '').substr(0, 10);

    return amount.charAt(amount.length - 1) === '.' ? `${parseFloat(amount)}.` : (parseFloat(amount) || 0);
  }

  setConvertedResults(amountFrom, amountTo, exchangeDirectionIsInverted) {
    this.setState({
      amountFrom,
      amountTo,
      exchangeDirectionIsInverted
    })
  }

  changeExchangeFromCurrency(c) {
    this.setState({exchangeFromCurrency: c})
    this.props.dispatch(setCurrencyFrom(c))

    if (this.state.exchangeDirectionIsInverted) {
      this.setState({
        amountFrom: this.convert(this.state.amountTo, this.state.exchangeToCurrency, c)
      })

    } else {
      this.setState({
        amountTo: this.convert(this.state.amountFrom, c, this.state.exchangeToCurrency)
      })
    }
  }

  changeExchangeToCurrency(c) {
    this.setState({exchangeToCurrency: c})

    if (this.state.exchangeDirectionIsInverted) {
      this.setState({
        amountFrom: this.convert(this.state.amountTo, c, this.state.exchangeFromCurrency)
      })

    } else {
      this.setState({
        amountTo: this.convert(this.state.amountFrom, this.state.exchangeFromCurrency, c)
      })
    }
  }

  updateExchangeResults() {
    if (this.state.exchangeDirectionIsInverted) {
      this.setExchange(this.state.amountTo, true);

    } else {
      this.setExchange(this.state.amountFrom);
    }
  }

  exchangeWallets = () => {
    const { exchangeFromCurrency, exchangeToCurrency, amountFrom, amountTo } = this.state;

    this.props.dispatch(exchange({
      exchangeFrom: {
        currency: exchangeFromCurrency,
        amount: amountFrom
      },
      exchangeTo: {
        currency: exchangeToCurrency,
        amount: amountTo
      }
    }));

    this.setState({
      amountFrom: 0,
      amountTo: 0
    });
  };

  render() {
    const { wallets, rates } = this.props;
    const { exchangeFromCurrency, exchangeToCurrency, amountFrom, amountTo } = this.state;
    const isEnoughFunds = amountFrom < wallets[exchangeFromCurrency];
    const isExchangeAllowed = (exchangeFromCurrency !== exchangeToCurrency) && isEnoughFunds && amountFrom > 0;

    return (
      <div className="page__i exchange">

        {Object.keys(rates).length && (
          <div className="exchange__rate">
            <Link className="exchange__rate__link" to={registry.rates}>
              1 {humanizeCurrency(exchangeFromCurrency)}&nbsp;
              = {this.convert(1, exchangeFromCurrency, exchangeToCurrency)} {humanizeCurrency(exchangeToCurrency)}
            </Link>
          </div>
        )}

        <div className="exchange__sections">
          <div className="exchange__section">
            <div className="exchange__currency">
              {exchangeFromCurrency}
              <span className="exchange__currency__left">
                You have {humanizeCurrency(exchangeFromCurrency)}{wallets[exchangeFromCurrency]}
              </span>

              <CurrenciesList
                wallets={wallets}
                setActive={(c) => this.changeExchangeFromCurrency(c)}
                activeCurrency={exchangeFromCurrency} />

            </div>

            <div className="exchange__input">
              <span className="exchange__input__symbol">-</span>
              <input type="text"
                     onChange={(e) => this.setExchange(e.target.value, false)}
                     value={amountFrom} />

              {!isEnoughFunds && (
                <span className='exchange__input__text'>You have not enough funds :(</span>
              )}
            </div>
          </div>

          <div className="exchange__section">
            <div className="exchange__currency">
              {exchangeToCurrency}
              <span className="exchange__currency__left">
                You have {humanizeCurrency(exchangeToCurrency)}{wallets[exchangeToCurrency]}
              </span>

              <CurrenciesList
                wallets={wallets}
                setActive={(c) => this.changeExchangeToCurrency(c)}
                activeCurrency={exchangeToCurrency} />

            </div>

            <div className="exchange__input">
              <span className="exchange__input__symbol">+</span>
              <input type="text"
                     onChange={(e) => this.setExchange(e.target.value, true)}
                     value={amountTo} />

              {Object.keys(rates).length && (
                <span className="exchange__input__text">
                  1 {humanizeCurrency(exchangeToCurrency)}&nbsp;
                  = {this.convert(1, exchangeToCurrency, exchangeFromCurrency)} {humanizeCurrency(exchangeFromCurrency)}
                </span>
              )}

            </div>
          </div>
        </div>
        <div className="exchange__submit">
          <button className="btn" disabled={!isExchangeAllowed} onClick={this.exchangeWallets}>Exchange</button>
        </div>
      </div>
    );
  }
}

Exchange.propTypes = {
  profile: PropTypes.object,
  wallets: PropTypes.object,
  rates: PropTypes.object,
  exchange_from: PropTypes.string,
  default_currency: PropTypes.string
};

export default connect(store => ({
  profile: store.profile,
  wallets: store.wallets,
  rates: store.rates.data.rates,
  exchange_from: store.exchange_from,
  default_currency: store.default_currency
}))(Exchange);

