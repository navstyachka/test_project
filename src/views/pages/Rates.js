import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import fx from 'money'

import FullCurrenciesList from '../components/FullCurrenciesList'

class Rates extends Component {
  state = {
    currencyPopupIsOpen: false,
    additionalCurrencies: []
  };

  convert(from, to) {
    const { rates, default_currency } = this.props;

    fx.rates = rates;
    fx.base = default_currency;

    return parseFloat(fx.convert(1, { from: from, to: to }).toFixed(2));
  }

  showCurrenciesPopup = () => {
    this.setState({
      currencyPopupIsOpen: true
    });
  };

  addCurrency = (c) => {
    this.setState({
      currencyPopupIsOpen: false,
      additionalCurrencies: this.state.additionalCurrencies.concat([c])
    });
  };

  renderCurrency(c) {
    const { exchange_from } = this.props;

    return (
      <div key={c} className="list__item">
        <span>1 {exchange_from}</span>
        <span>{this.convert(exchange_from, c)} {c}</span>
      </div>
    );
  }

  render() {
    const { wallets, exchange_from, rates } = this.props;
    const { additionalCurrencies } = this.state;

    return (
      <div className="page__i rates">
        <div className="list">
          <div className="list__title">Rates:</div>

          {Object.keys(wallets).map((currency) => (
            exchange_from !== currency && this.renderCurrency(currency)
          ))}

          {additionalCurrencies.map((currency) => this.renderCurrency(currency))}

        </div>

        <div className="rates__currencies">
          <button className="btn" onClick={this.showCurrenciesPopup}>Add new currency</button>
        </div>

        {this.state.currencyPopupIsOpen && (
          <FullCurrenciesList
            addCurrency={(c) => this.addCurrency(c)}
            rates={rates}
            closePopup={() => this.setState({ currencyPopupIsOpen: false })} />
        )}

      </div>
    );
  }
}

Rates.propTypes = {
  default_currency: PropTypes.string,
  exchange_from: PropTypes.string,
  rates: PropTypes.object,
  wallets: PropTypes.object
};

export default connect(store => ({
  default_currency: store.default_currency,
  exchange_from: store.exchange_from,
  rates: store.rates.data.rates,
  wallets: store.wallets
}))(Rates)