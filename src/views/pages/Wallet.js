import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import registry from '../../registry';
import { setCurrencyFrom } from '../../actions';
import { humanizeCurrency } from '../../utilities';

class Wallet extends Component {
  activeIndex = 0;

  state = {
    walletsCurrencies: []
  };

  componentDidMount() {
    this.setState({
      walletsCurrencies: Object.keys(this.props.wallets).map((w) => w)
    });
  }

  setActiveCurrency(direction) {
    if (direction === 'next') {
      if (this.activeIndex + 1 === this.state.walletsCurrencies.length) {
        this.activeIndex = 0;
      } else {
        this.activeIndex = this.activeIndex + 1;
      }

    } else {
      if (this.activeIndex - 1 < 0) {
        this.activeIndex = this.state.walletsCurrencies.length - 1;
      } else {
        this.activeIndex = this.activeIndex - 1;
      }

    }

    this.props.dispatch(setCurrencyFrom(
      this.state.walletsCurrencies[this.activeIndex]
    ));
  }

  render() {
    const { walletsCurrencies } = this.state;
    const { wallets, exchange_from } = this.props;

    if (!walletsCurrencies) {
      return (
        <div className="page__i wallets">
          <div className="loading">Loading...</div>
        </div>
      );
    }

    return (
      <div className="page__i wallets">
        <div className="wallets__list">

          {walletsCurrencies.map((item, i) => {
            const className = exchange_from === item ? 'wallets__item active' : 'wallets__item';

            return (
              <div key={i} className={className}>
                {humanizeCurrency(item)}{wallets[item]}
                {exchange_from === item && <span className="wallets__item__curr">{item}</span>}
              </div>
            );
          })}

        </div>
        <div className="wallets__prev prev" onClick={() => this.setActiveCurrency('prev')}/>
        <div className="wallets__next next" onClick={() => this.setActiveCurrency('next')}/>
        <div className="wallets__exchange">
          <Link to={registry.exchange} className='btn'>Exchange</Link>
        </div>
      </div>
    );
  }
}

Wallet.propTypes = {
  wallets: PropTypes.object,
  exchange_from: PropTypes.string
};

export default connect(store => ({
  wallets: store.wallets,
  exchange_from: store.exchange_from
}))(Wallet);